﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;

[assembly: ExportRenderer(typeof(Krusbar.SettingsPage), typeof(Krusbar.iOS.SettingsPage))]
namespace Krusbar.iOS
{
    public class SettingsPage : PageRenderer
    {
        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            setNavBarColor();
        }

        void setNavBarColor()
        {
            NavigationController.NavigationBar.ClipsToBounds = true;

            var backImage = new UIImage("nav_bar_back");
            NavigationController.NavigationBar.SetBackgroundImage(backImage, UIBarMetrics.Default);
        }
    }
}
