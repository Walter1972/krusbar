﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreGraphics;
using CoreAnimation;

[assembly: ExportRenderer(typeof(Krusbar.CardEnquiryPage), typeof(Krusbar.iOS.CardEnquiryPage))]
namespace Krusbar.iOS
{
    public class CardEnquiryPage : PageRenderer
    {
        //CAGradientLayer gradientLayer;

        /*
        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            MessagingCenter.Subscribe<Krusbar.CardEnquiryPage>(this, "removeNavBarItems", (s) => {
                //gradientLayer.RemoveFromSuperLayer();

                var a = (Krusbar.CardEnquiryPage)Element;
                a.removeNavBarItemsDone();
            });
        }
        */

		public override void ViewWillAppear(bool animated)
		{
			base.ViewWillAppear(animated);

            //https://stackoverflow.com/questions/28848545/add-gradient-background-to-layouts-in-xamarin-forms-visual-studio
            //https://forums.xamarin.com/discussion/22440/gradient-as-background-color

            setNavBarColor();
		}

        void setNavBarColor() {
            /*
            var StartColor = new CGColor(0.0f, 169.0f / 255.0f, 157.0f / 255.0f, 1.0f);
            var CenterColor = new CGColor(0.0f, 126.0f / 255.0f, 142.0f / 255.0f, 1.0f);
            var EndColor = new CGColor(0.0f, 83.0f / 255.0f, 126.0f / 255.0f, 1.0f);

            if (gradientLayer == null) {
                gradientLayer = new CAGradientLayer();
            }

            gradientLayer.Frame = NavigationController.NavigationBar.Bounds;
            gradientLayer.Colors = new CGColor[] { StartColor, CenterColor, EndColor };
            
            gradientLayer.RemoveFromSuperLayer();
            NavigationController.NavigationBar.Layer.InsertSublayer(gradientLayer, 0);

            var layers = NavigationController.NavigationBar.Layer.Sublayers;
            //NavigationController.NavigationBar.Layer.InsertSublayerBelow(gradientLayer, NavigationController.NavigationBar.Layer.Sublayers[0]);
            */

            NavigationController.NavigationBar.ClipsToBounds = true;

            var backImage = new UIImage("nav_bar_back");
            NavigationController.NavigationBar.SetBackgroundImage(backImage, UIBarMetrics.Default);
        }
    }
}
