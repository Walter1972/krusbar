﻿using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using CoreGraphics;
using CoreAnimation;

[assembly: ExportRenderer(typeof(Krusbar.CardEnquiryInfoPage), typeof(Krusbar.iOS.CardEnquiryInfoPage))]
namespace Krusbar.iOS
{
    public class CardEnquiryInfoPage : PageRenderer
    {
        UISegmentedControl segmentControl;
        //CAGradientLayer gradientLayer;
        UIButton backButton;

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);

            //setNavBarSegment();
            setNavBarColor();
            setBackButton();
        }

        void removeNavBarItems()
        {
            //removeNavBarSegment();
            //gradientLayer.RemoveFromSuperLayer();
            backButton.RemoveFromSuperview();
        }

        void setNavBarColor()
        {
            /*
            var StartColor = new CGColor(0.0f, 169.0f / 255.0f, 157.0f / 255.0f, 1.0f);
            var CenterColor = new CGColor(0.0f, 126.0f / 255.0f, 142.0f / 255.0f, 1.0f);
            var EndColor = new CGColor(0.0f, 83.0f / 255.0f, 126.0f / 255.0f, 1.0f);

            gradientLayer = new CAGradientLayer();
            gradientLayer.Frame = NavigationController.NavigationBar.Bounds;
            gradientLayer.Colors = new CGColor[] { StartColor, CenterColor, EndColor };
            NavigationController.NavigationBar.Layer.InsertSublayer(gradientLayer, 0);

            //var layers = NavigationController.NavigationBar.Layer.Sublayers;
            //NavigationController.NavigationBar.Layer.InsertSublayerBelow(gradientLayer, NavigationController.NavigationBar.Layer.Sublayers[0]);
            */

            NavigationController.NavigationBar.ClipsToBounds = true;

            var backImage = new UIImage("nav_bar_back");
            NavigationController.NavigationBar.SetBackgroundImage(backImage, UIBarMetrics.Default);
        }

        void setBackButton()
        {
            NavigationController.TopViewController.NavigationItem.LeftBarButtonItem = null;
            NavigationController.TopViewController.NavigationItem.HidesBackButton = true;

            UIImage backImage = new UIImage("arrow_back");

            CGRect navBarFrame = NavigationController.NavigationBar.Frame;

            backButton = new UIButton(UIButtonType.Custom);
            backButton.SetImage(backImage, UIControlState.Normal);
            backButton.Frame = new CGRect(10.0, (navBarFrame.Height - backImage.Size.Height) * 0.5, backImage.Size.Width, backImage.Size.Height);
            backButton.TouchUpInside += (sender, e) => {
                removeNavBarItems();
                NavigationController.PopViewController(true);
            };

            NavigationController.NavigationBar.AddSubview(backButton);
        }

        void setNavBarSegment()
        {
            CGRect navBarFrame = NavigationController.NavigationBar.Frame;
            NavigationController.NavigationBar.Frame = new CGRect(navBarFrame.X, navBarFrame.Y, navBarFrame.Width, 88.0);

            segmentControl = new UISegmentedControl();
            segmentControl.Frame = new CGRect(0.0, 54.0, navBarFrame.Width, 34.0);

            segmentControl.InsertSegment("E-TICKET", 0, false);
            segmentControl.InsertSegment("PURSE", 1, false);
            segmentControl.InsertSegment("PASS", 1, false);
            segmentControl.SelectedSegment = 2;

            segmentControl.ValueChanged += (sender, e) => {
                var selectedSegmentId = (sender as UISegmentedControl).SelectedSegment;
                // do something with selectedSegmentId

                //var a = (Krusbar.CardEnquiryInfoPage)Element;
                //a.SegmentChanged((int)selectedSegmentId);
            };

            NavigationController.NavigationBar.AddSubview(segmentControl);
        }

        void removeNavBarSegment()
        {
            //gradientLayer.RemoveFromSuperLayer();
            segmentControl.RemoveFromSuperview();
            backButton.RemoveFromSuperview();

            CGRect navBarFrame = this.NavigationController.NavigationBar.Frame;
            this.NavigationController.NavigationBar.Frame = new CGRect(navBarFrame.X, navBarFrame.Y, navBarFrame.Width, 44.0);
        }
    }
}
