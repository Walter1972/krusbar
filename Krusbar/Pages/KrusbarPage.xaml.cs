﻿using Xamarin.Forms;
using Krusbar.MenuItems;
using System;

namespace Krusbar
{
	public partial class KrusbarPage : MasterDetailPage
	{
        //MasterPage masterPage;

		public KrusbarPage()
		{
			InitializeComponent();

			masterPage = new MasterPage();
			Master = masterPage;
            Detail = new NavigationPage(new CardEnquiryPage());
            /*
            {
                BarBackgroundColor = Color.FromHex("CC0066"),
                BarTextColor = Color.FromHex("FFFFFF")
            };
            */
            masterPage.ListView.ItemSelected += OnItemSelected;
            //masterPage.Appearing += OnMenuOpen;
		}

		void OnItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			var item = e.SelectedItem as MasterPageItem;
			if (item != null)
			{
				Detail = new NavigationPage((Page)Activator.CreateInstance(item.TargetType));
                masterPage.updateSelectedItem();
				masterPage.ListView.SelectedItem = null;
				IsPresented = false;
			}
		}
	}
}
