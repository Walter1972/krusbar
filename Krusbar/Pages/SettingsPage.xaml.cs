﻿using Xamarin.Forms;
using Krusbar.Cells;
using System.Collections.ObjectModel;
using System;

namespace Krusbar
{
    public partial class SettingsPage : ContentPage, IDisposable
    {
        bool scannerOn;
        ToolbarItem rightToolbarItem;

        SettingsItemTemplate itemSwe;
        SettingsItemTemplate itemEng;

        TapGestureRecognizer tapGestureRecognizerSwe;
        TapGestureRecognizer tapGestureRecognizerEng;

        public SettingsPage()
        {
            InitializeComponent();

            Title = "Settings";
            BackgroundColor = Color.FromRgb(244, 244, 244);

            scannerOn = true;

            setRightBarButton(scannerOn);

            addContent();
        }

        void setRightBarButton(bool isOn)
        {
            if (rightToolbarItem != null)
            {
                ToolbarItems.Remove(rightToolbarItem);
            }

            string iconName = isOn ? "net_on" : "net_off";

            rightToolbarItem = new ToolbarItem("Scanner", iconName, () => {
                scannerOn = !scannerOn;
                setRightBarButton(scannerOn);
            });

            ToolbarItems.Add(rightToolbarItem);
        }

        void addContent()
        {
            var topLabel = new Label
            {
                Text = "Language",
                TextColor = Color.FromHex("#4b4b4b"),
                FontSize = 18,
                Margin = new Thickness(20, 20, 0, 0)
            };

            itemSwe = new SettingsItemTemplate()
            {
                HeightRequest = 90,
                IconName = "swe",
                LanguageName = "Swedish",
                IsSelected = false
            };
            tapGestureRecognizerSwe = new TapGestureRecognizer();
            tapGestureRecognizerSwe.Tapped += (s, e) => {
                itemSwe.IsSelected = true;
                itemEng.IsSelected = false;
            };
            itemSwe.GestureRecognizers.Add(tapGestureRecognizerSwe);

            itemEng = new SettingsItemTemplate()
            {
                HeightRequest = 90,
                IconName = "eng",
                LanguageName = "English",
                IsSelected = true
            };
            tapGestureRecognizerEng = new TapGestureRecognizer();
            tapGestureRecognizerEng.Tapped += (s, e) => {
                itemSwe.IsSelected = false;
                itemEng.IsSelected = true;
            };
            itemEng.GestureRecognizers.Add(tapGestureRecognizerEng);

            var bottomLine = new BoxView
            {
                HeightRequest = 1,
                BackgroundColor = Color.FromHex("#dadada")
            };

            var stack = new StackLayout
            {
                VerticalOptions = LayoutOptions.Start,
                Orientation = StackOrientation.Vertical,
                Children = { topLabel, itemSwe, itemEng, bottomLine }
            };

            Content = stack;
        }

        public void Dispose()
        {
            itemSwe.GestureRecognizers.Remove(tapGestureRecognizerSwe);
            itemEng.GestureRecognizers.Remove(tapGestureRecognizerEng);
        }
    }
}
