﻿using Xamarin.Forms;
using Krusbar.Cells;
using System.Collections.ObjectModel;

namespace Krusbar
{
    public partial class CardEnquiryInfoPage : ContentPage
    {
        StackLayout segmentLayout;
        StackLayout stackMiddle;

        ListView listViewETicket;
        ObservableCollection<ETicket> listETicket;

        ListView listViewPurse;
        ObservableCollection<Purse> listPurse;

        ListView listViewPass;
        ObservableCollection<Pass> listPass;

        public CardEnquiryInfoPage()
        {
            InitializeComponent();

            Title = "Card Enquiry";
            BackgroundColor = Color.FromRgb(244, 244, 244);

            setUpListViewETicket();
            setUpListViewPurse();
            setUpListViewPass();

            addContent();
        }

        void setUpSegmentLayout()
        {
            Button btnTicket = new Button()
            {
                Text = "E-TICKET"
            };
            Button btnPurse = new Button()
            {
                Text = "PURSE"
            };
            Button btnPass = new Button()
            {
                Text = "PASS"
            };

            Color segmentColor = SegmentControl.mainColor;

            SegmentControl segmentcontrol = new SegmentControl(true)
            {
                SelectedSegment = 0,
                TintColor = segmentColor
            };
            segmentLayout = new StackLayout()
            {
                HorizontalOptions = LayoutOptions.FillAndExpand
            };
            segmentLayout.Children.Add(segmentcontrol);

            segmentcontrol.AddSegment(btnTicket, segmentColor);
            segmentcontrol.AddSegment(btnPurse, segmentColor);
            segmentcontrol.AddSegment(btnPass, segmentColor);

            segmentcontrol.SelectedSegmentChanged += (sender, e) =>
            {
                SegmentChanged(e);
            };
        }

        void addContent()
        {
            setUpSegmentLayout();

            var bottomImage = new Image();
            bottomImage.Source = "lines_bg";
            bottomImage.Margin = new Thickness(0.0, 15.0, 0.0, 15.0);

            var stackTop = new StackLayout
            {
                VerticalOptions = LayoutOptions.Start,
                Orientation = StackOrientation.Vertical,
                Children = { segmentLayout }
            };

            stackMiddle = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Orientation = StackOrientation.Vertical,
                Children = { listViewETicket }
            };

            var stackBottom = new StackLayout
            {
                VerticalOptions = LayoutOptions.End,
                Orientation = StackOrientation.Vertical,
                Children = { bottomImage }
            };

            var stack = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Orientation = StackOrientation.Vertical,
                Children = { stackTop, stackMiddle, stackBottom }
            };

            Content = stack;
        }

        public void SegmentChanged(int index)
        {
            if (index == 0)
            {
                stackMiddle.Children.Clear();
                stackMiddle.Children.Add(listViewETicket);
            }
            else if (index == 1)
            {
                stackMiddle.Children.Clear();
                stackMiddle.Children.Add(listViewPurse);
            }
            else
            {
                stackMiddle.Children.Clear();
                stackMiddle.Children.Add(listViewPass);
            }
        }

        void setUpListViewETicket()
        {
            listETicket = new ObservableCollection<ETicket>();

            var eTicketCellTemplate = new DataTemplate(typeof(ETicketCellTemplate));
            eTicketCellTemplate.SetBinding(ETicketCellTemplate.GroupProperty, "Group");
            eTicketCellTemplate.SetBinding(ETicketCellTemplate.PriceProperty, "Price");
            eTicketCellTemplate.SetBinding(ETicketCellTemplate.DurationProperty, "Duration");

            listViewETicket = new ListView();
            listViewETicket.RowHeight = 180;
            listViewETicket.ItemTemplate = eTicketCellTemplate;
            listViewETicket.ItemsSource = listETicket;
            listViewETicket.BackgroundColor = Color.Transparent;
            listViewETicket.SeparatorVisibility = SeparatorVisibility.None;

            setListETicket();
        }

        void setListETicket()
        {
            listETicket.Clear();
            listETicket.Add(new ETicket()
            {
                Group = "2 Adt. / 1Disc.",
                Price = 0.0,
                Duration = 66.0
            });
            listETicket.Add(new ETicket()
            {
                Group = "3 Adt. / 1Disc.",
                Price = 110.0,
                Duration = 60.0
            });
        }

        void setUpListViewPurse()
        {
            listPurse = new ObservableCollection<Purse>();

            var purseCellTemplate = new DataTemplate(typeof(PurseCellTemplate));
            purseCellTemplate.SetBinding(PurseCellTemplate.PriceProperty, "Price");
            purseCellTemplate.SetBinding(PurseCellTemplate.WalletProperty, "Wallet");
            purseCellTemplate.SetBinding(PurseCellTemplate.PersonProperty, "Person");

            listViewPurse = new ListView();
            listViewPurse.RowHeight = 180;
            listViewPurse.ItemTemplate = purseCellTemplate;
            listViewPurse.ItemsSource = listPurse;
            listViewPurse.BackgroundColor = Color.Transparent;
            listViewPurse.SeparatorVisibility = SeparatorVisibility.None;

            setListPurse();
        }

        void setListPurse()
        {
            listPurse.Clear();
            listPurse.Add(new Purse()
            {
                Price = 25.0,
                Wallet = "Not Blocked",
                Person = "Adult"
            });
            listPurse.Add(new Purse()
            {
                Price = 15.0,
                Wallet = "Not Blocked",
                Person = "Student"
            });
        }

        void setUpListViewPass()
        {
            listPass = new ObservableCollection<Pass>();

            var passCellTemplate = new DataTemplate(typeof(PassCellTemplate));
            passCellTemplate.SetBinding(PassCellTemplate.TicketTypeProperty, "TicketType");
            passCellTemplate.SetBinding(PassCellTemplate.TicketCategoryProperty, "TicketCategory");
            passCellTemplate.SetBinding(PassCellTemplate.TicketStatusProperty, "TicketStatus");
            passCellTemplate.SetBinding(PassCellTemplate.PriceProperty, "Price");
            passCellTemplate.SetBinding(PassCellTemplate.DateProperty, "Date");

            listViewPass = new ListView();
            listViewPass.RowHeight = 180 + 40;
            listViewPass.ItemTemplate = passCellTemplate;
            listViewPass.ItemsSource = listPass;
            listViewPass.BackgroundColor = Color.Transparent;
            listViewPass.SeparatorVisibility = SeparatorVisibility.None;

            setListPass();
        }

        void setListPass()
        {
            listPass.Clear();

            listPass.Add(new Pass()
            {
                TicketType = "30 Days ticket Re",
                TicketCategory = "Adt.",
                TicketStatus = "Not blocked",
                Price = 490.0,
                Date = "2012-09-03"
            });
            listPass.Add(new Pass()
            {
                TicketType = "10 Days ticket Re",
                TicketCategory = "Adt.",
                TicketStatus = "Not blocked",
                Price = 150.0,
                Date = "2017-10-27"
            });
        }
    }
}
