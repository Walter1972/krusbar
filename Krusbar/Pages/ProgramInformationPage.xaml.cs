﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Krusbar
{
    public partial class ProgramInformationPage : ContentPage
    {
        bool scannerOn;
        ToolbarItem rightToolbarItem;

        public ProgramInformationPage()
        {
            InitializeComponent();

            Title = "Program Information";
            BackgroundColor = Color.FromRgb(244, 244, 244);

            scannerOn = true;

            setRightBarButton(scannerOn);
            addContent();
        }

        void setRightBarButton(bool isOn)
        {
            if (rightToolbarItem != null)
            {
                ToolbarItems.Remove(rightToolbarItem);
            }

            string iconName = isOn ? "net_on" : "net_off";

            rightToolbarItem = new ToolbarItem("Scanner", iconName, () => {
                scannerOn = !scannerOn;
                setRightBarButton(scannerOn);
            });

            ToolbarItems.Add(rightToolbarItem);
        }

        void addContent()
        {
            //1
            LabelNameTitle.TextColor = Color.FromHex("#404040");
            LabelNameTitle.FontSize = 18;

            LabelName.TextColor = Color.FromHex("#404040");
            LabelName.FontSize = 18;
            LabelName.FontAttributes = FontAttributes.Bold;

            //2
            LabelVersionTitle.TextColor = Color.FromHex("#404040");
            LabelVersionTitle.FontSize = 18;

            LabelVersion.TextColor = Color.FromHex("#404040");
            LabelVersion.FontSize = 18;
            LabelVersion.FontAttributes = FontAttributes.Bold;

            //3
            LabelAndradTitle.TextColor = Color.FromHex("#404040");
            LabelAndradTitle.FontSize = 18;

            LabelAndrad.TextColor = Color.FromHex("#404040");
            LabelAndrad.FontSize = 18;
            LabelAndrad.FontAttributes = FontAttributes.Bold;

            //4
            LabelBuildTitle.TextColor = Color.FromHex("#404040");
            LabelBuildTitle.FontSize = 18;

            LabelBuild.TextColor = Color.FromHex("#404040");
            LabelBuild.FontSize = 18;
            LabelBuild.FontAttributes = FontAttributes.Bold;

            //5
            LabelCDVersionTitle.TextColor = Color.FromHex("#404040");
            LabelCDVersionTitle.FontSize = 18;

            LabelCDVersion.TextColor = Color.FromHex("#404040");
            LabelCDVersion.FontSize = 18;
            LabelCDVersion.FontAttributes = FontAttributes.Bold;

            //6
            LabelActionListTitle.TextColor = Color.FromHex("#404040");
            LabelActionListTitle.FontSize = 18;

            LabelActionList.TextColor = Color.FromHex("#404040");
            LabelActionList.FontSize = 18;
            LabelActionList.FontAttributes = FontAttributes.Bold;

            //7
            LabelCopyrightTitle.TextColor = Color.FromHex("#7d7d7d");
            LabelCopyrightTitle.FontSize = 14;

            LabelSiteLink.TextColor = Color.FromHex("#568ac2");
            LabelSiteLink.FontSize = 14;

            var tapGestureRecognizerOpenSite = new TapGestureRecognizer();
            tapGestureRecognizerOpenSite.Tapped += (s, e) => {
                Device.OpenUri(new Uri("http://www.modulsystem.se"));
            };
            LabelSiteLink.GestureRecognizers.Add(tapGestureRecognizerOpenSite);
        }
    }
}
