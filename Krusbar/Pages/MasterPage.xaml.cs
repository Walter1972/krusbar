﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Krusbar.MenuItems;

namespace Krusbar
{
    public partial class MasterPage : ContentPage
    {
        public ListView ListView { get { return listView; } }
        private MasterPageItem selectedItem;

        public MasterPage()
        {
            InitializeComponent();
            
			var masterPageItems = new List<MasterPageItem>();

            MasterPageItem item1 = new MasterPageItem {
                Title = "Validation",
                IconName = "validation",
                IconSource = "validation.png",
                TargetType = typeof(ValidationPage)
            };

            MasterPageItem item2 = new MasterPageItem {
                Title = "Card Enquiry",
                IconName = "card_enquiry",
                IconSource = "card_enquiry.png",
                TargetType = typeof(CardEnquiryPage)
            };

			MasterPageItem item3 = new MasterPageItem {
				Title = "Settings",
				IconName = "settings",
				IconSource = "settings.png",
				TargetType = typeof(SettingsPage)
			};

			MasterPageItem item4 = new MasterPageItem {
				Title = "Program Information",
                IconName = "info",
				IconSource = "info.png",
                TargetType = typeof(ProgramInformationPage)
			};

			MasterPageItem item5 = new MasterPageItem {
				Title = "Exit",
                IconName = "logout",
				IconSource = "logout.png",
				TargetType = typeof(CardEnquiryPage)
			};

            masterPageItems.Add(item1);
            masterPageItems.Add(item2);
            masterPageItems.Add(item3);
            masterPageItems.Add(item4);
            masterPageItems.Add(item5);

			listView = new ListView
			{
				ItemsSource = masterPageItems,
				ItemTemplate = new DataTemplate(() => {
                    var grid = new Grid { Padding = new Thickness(5.0, 20.0, 0.0, 0.0) };// (15, 10)
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = new GridLength(50) });
					grid.ColumnDefinitions.Add(new ColumnDefinition { Width = GridLength.Star });

					var image = new Image();
					image.SetBinding(Image.SourceProperty, "IconSource");
					var label = new Label { VerticalOptions = LayoutOptions.FillAndExpand };
                    label.SetBinding(Label.TextProperty, "Title");
                    label.SetBinding(Label.TextColorProperty, new Binding("TextColor"));

					grid.Children.Add(image);
					grid.Children.Add(label, 1, 0);

					return new ViewCell { View = grid };
				}),
				SeparatorVisibility = SeparatorVisibility.None
			};

            logoImage = new Image
            {
                Source = "logo.png",
                Margin = new Thickness(0.0, 60.0, 0.0, 10.0)
                //BackgroundColor = Color.White
                                       /*
                WidthRequest = 250.0,
                MinimumWidthRequest = 250.0,
                HeightRequest = 34.0,
                MinimumHeightRequest = 34.0
                */
            };

            logoLabel = new Label
            {
                Text = "Modul-Sytem",
                HorizontalTextAlignment = TextAlignment.Center
            };

            GradientColorStack topStack = new GradientColorStack
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,
                VerticalOptions = LayoutOptions.FillAndExpand,
                StartColor = Color.FromHex("#00a99d"),
                CenterColor = Color.FromHex("#007e8e"),
                EndColor = Color.FromHex("#00537e"),
                HeightRequest = 150.0,
                MinimumHeightRequest = 150.0,
                Children = { logoImage, logoLabel }
            };

            var marginTop = 0.0;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    marginTop = -20.0;
                    break;
                case Device.Android:
                    marginTop = -40.0;
                    break;
                case Device.WinPhone:
                    marginTop = 0.0;
                    break;
            }

			Content = new StackLayout {
                Children = { topStack, listView },
                Margin = new Thickness(0.0, marginTop, 0.0, 0.0)
			};

            item1.SetSelected(false);
            item3.SetSelected(false);
            item4.SetSelected(false);
            item5.SetSelected(false);

            selectedItem = item2;
            selectedItem.SetSelected(true);
        }

        public void updateSelectedItem() {
			// Deselect previous
			if (selectedItem != null)
			{
                selectedItem.SetSelected(false);
			}

			// Select new
			selectedItem = (ListView.SelectedItem as MasterPageItem);
            selectedItem.SetSelected(true);
        }
    }
}
