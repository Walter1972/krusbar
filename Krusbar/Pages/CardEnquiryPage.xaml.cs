﻿using Xamarin.Forms;

namespace Krusbar
{
    public partial class CardEnquiryPage : ContentPage
    {
        bool scannerOn;
        ToolbarItem rightToolbarItem;
        Button scanButton;
        
        const int BUTTON_BORDER_WIDTH = 1;
        const int BUTTON_HEIGHT = 200;
        const int BUTTON_HALF_HEIGHT = 100;
        const int BUTTON_WIDTH = 200;

        public CardEnquiryPage()
        {
            InitializeComponent();

            Title = "Card Enquiry";
            BackgroundColor = Color.FromRgb(244, 244, 244);

			scannerOn = true;

			//setRightBarButton(scannerOn);
            addContent();
        }

		void setRightBarButton(bool isOn)
		{
            if (rightToolbarItem != null) {
                ToolbarItems.Remove(rightToolbarItem);
            }

            string iconName = isOn ? "net_on.png" : "net_off.png";

            rightToolbarItem = new ToolbarItem("Scanner", iconName, () => {
                scannerOn = !scannerOn;
                setRightBarButton(scannerOn);
            });

			ToolbarItems.Add(rightToolbarItem);
		}

        void addContent() {
            scanButton = new Button
            {
                HorizontalOptions = LayoutOptions.Center,
                BorderColor = Color.FromRgba(0.0, 0.0, 0.0, 0.25),
                BorderWidth = BUTTON_BORDER_WIDTH,
                BorderRadius = BUTTON_HALF_HEIGHT,
                HeightRequest = BUTTON_HEIGHT,
                MinimumHeightRequest = BUTTON_HEIGHT,
                WidthRequest = BUTTON_WIDTH,
                MinimumWidthRequest = BUTTON_WIDTH,
                Image = "switcher_on.png",
                Command = new Command(() => {
                    //MessagingCenter.Send<Krusbar.CardEnquiryPage>(this, "removeNavBarItems");

                    //scanButton.Image = "switcher_on.png";

                    var cardEnquiryInfoPage = new CardEnquiryInfoPage();
                    Navigation.PushAsync(cardEnquiryInfoPage, true);
                })
            };
            //scanButton.IsEnabled = false;

            double labelTopMargin;

            switch (Device.RuntimePlatform)
            {
                case Device.iOS:
                    scanButton.BackgroundColor = Color.White;
                    labelTopMargin = 15.0;
                    break;

                default:
                    scanButton.BackgroundColor = Color.FromRgba(0, 0, 0, 0);
                    labelTopMargin = 0.0;
                    break;
            }

            var label = new Label
            {
                HorizontalOptions = LayoutOptions.Center,
                Margin = new Thickness(0.0, labelTopMargin, 0.0, 0.0),
                TextColor = Color.FromRgb(75, 75, 75),
                Text = "You can start scan card"
            };

            var stack = new StackLayout
            {
                VerticalOptions = LayoutOptions.Center,
                Orientation = StackOrientation.Vertical,
                Children = { scanButton, label },
            };

            Content = stack;
        }
        /*
        public void removeNavBarItemsDone()
        {
            var cardEnquiryInfoPage = new CardEnquiryInfoPage();
            Navigation.PushAsync(cardEnquiryInfoPage, true);
        }
        */
    }
}
