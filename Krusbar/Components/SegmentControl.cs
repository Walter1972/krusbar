﻿using System;

using Xamarin.Forms;

namespace Krusbar
{
    //https://forums.xamarin.com/discussion/23880/segmented-control-for-android

    public class SegmentControl : ContentView
    {
        public SegmentControl(bool IsExpand)
        {
            this.isexpand = IsExpand;
            layout = new StackLayout
            {
                Orientation = StackOrientation.Horizontal,
                Padding = new Thickness(0),
                Spacing = 0,
            };
            if (isexpand)
                layout.HorizontalOptions = LayoutOptions.FillAndExpand;
            else
                layout.HorizontalOptions = LayoutOptions.Center;
            HorizontalOptions = LayoutOptions.FillAndExpand;
            VerticalOptions = LayoutOptions.StartAndExpand;
            Padding = new Thickness(0, 0);
            Content = layout;
            selectedSegment = 0;
            ClickedCommand = new Command(SetSelectedSegment);
        }

        public static Color mainColor
        {
            get
            {
                var color = Color.FromHex("#00537e");
                /*
                switch (Device.RuntimePlatform)
                {
                    case Device.iOS:
                        color = Color.FromHex("#006791");
                        break;
                    case Device.Android:
                        color = Color.FromHex("#00537e");
                        break;
                    case Device.WinPhone:
                        color = Color.FromHex("#00537e");
                        break;
                }
                */
                return color;
            }
        }

        StackLayout layout;
        Color tintColor = Color.Black;

        Color unselectedColor = mainColor;

        public bool isexpand
        {
            get;
            set;
        }

        public Color TintColor
        {
            get { return tintColor; }
            set
            {
                tintColor = value;
                if (layout == null)
                {
                    return;
                }
                layout.BackgroundColor = Color.White;
                for (var iBtn = 0; iBtn < layout.Children.Count; iBtn++)
                {
                    SetSelectedState(iBtn, iBtn == selectedSegment, true);
                }
            }
        }

        int selectedSegment;
        public int SelectedSegment
        {
            get
            {
                return selectedSegment;
            }
            set
            {
                if (value == selectedSegment)
                {
                    return;
                }
                SetSelectedState(selectedSegment, false);
                selectedSegment = value;
                if (value < 0 || value >= layout.Children.Count)
                {
                    return;
                }
                SetSelectedState(selectedSegment, true);
            }
        }

        public event EventHandler<int> SelectedSegmentChanged;
        public Command ClickedCommand;

        public void AddSegment(Button segment, Color othercolor)
        {
            //unselectedColor = othercolor;
            //tintColor = segment.BorderColor;
            segment.HorizontalOptions = LayoutOptions.FillAndExpand;
            if (isexpand)
                segment.HorizontalOptions = LayoutOptions.FillAndExpand;

            //if (App.IsTablet)
            //{
            //    segment.WidthRequest = Theme.ButtonWidth;
            //    segment.HeightRequest = Theme.ButtonHeight;
            //}

            segment.BorderRadius = 0;
            //segment.BorderWidth = 0;
            segment.TextColor = TintColor;
            segment.CommandParameter = layout.Children.Count;
            segment.Command = ClickedCommand;
            layout.BackgroundColor = TintColor;
            layout.Children.Add(segment);
            SetSelectedState(layout.Children.Count - 1, layout.Children.Count - 1 == selectedSegment);
        }

        void SetSelectedSegment(object o)
        {
            var selectedIndex = (int)o;
            SelectedSegment = selectedIndex;
            if (SelectedSegmentChanged != null)
            {
                SelectedSegmentChanged(this, selectedIndex);
            }
        }

        public void SetSegmentText(int iSegment, string segmentText)
        {
            if (iSegment >= layout.Children.Count || iSegment < 0)
            {
                throw new IndexOutOfRangeException("SetSegmentText: Attempted to change segment text for a segment doesn't exist.");
            }

            ((Button)layout.Children[iSegment]).Text = segmentText;
        }

        void SetSelectedState(int indexer, bool isSelected, bool setBorderColor = true)
        {
            if (layout.Children.Count <= indexer)
            {
                return; //Out of bounds
            }
            var button = (Button)layout.Children[indexer];
            if (isSelected)
            {
                button.BackgroundColor = tintColor;
                button.TextColor = Color.White;
            }
            else
            {
                button.BackgroundColor = unselectedColor;
                button.TextColor = Color.SlateGray;
            }
        }
    }
}

