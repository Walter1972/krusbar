﻿using System;
using Xamarin.Forms;

namespace Krusbar
{
    public class GradientColorStack : StackLayout
    {
        public Color StartColor { get; set; }
        public Color CenterColor { get; set; }
        public Color EndColor { get; set; }

        public GradientColorStack()
        {
        }
    }
}
