﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Krusbar.Cells
{
    public class Pass
    {
        public string TicketType { get; set; }
        public string TicketCategory { get; set; }
        public string TicketStatus { get; set; }
        public double Price { get; set; }
        public string Date { get; set; }
    }

    public partial class PassCellTemplate : ViewCell
    {
        public static readonly BindableProperty TicketTypeProperty = BindableProperty.Create("TicketType", typeof(string), typeof(PassCellTemplate), "");
        public static readonly BindableProperty TicketCategoryProperty = BindableProperty.Create("TicketCategory", typeof(string), typeof(PassCellTemplate), "");
        public static readonly BindableProperty TicketStatusProperty = BindableProperty.Create("TicketStatus", typeof(string), typeof(PassCellTemplate), "");
        public static readonly BindableProperty PriceProperty = BindableProperty.Create("Price", typeof(double), typeof(PassCellTemplate), 0.0);
        public static readonly BindableProperty DateProperty = BindableProperty.Create("Date", typeof(string), typeof(PassCellTemplate), "");

        public string TicketType
        {
            get { return (string)GetValue(TicketTypeProperty); }
            set { SetValue(TicketTypeProperty, value); }
        }

        public string TicketCategory
        {
            get { return (string)GetValue(TicketCategoryProperty); }
            set { SetValue(TicketCategoryProperty, value); }
        }

        public string TicketStatus
        {
            get { return (string)GetValue(TicketStatusProperty); }
            set { SetValue(TicketStatusProperty, value); }
        }

        public double Price
        {
            get { return (double)GetValue(PriceProperty); }
            set { SetValue(PriceProperty, value); }
        }

        public string Date
        {
            get { return (string)GetValue(DateProperty); }
            set { SetValue(DateProperty, value); }
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext != null)
            {
                LabelTicketType.Text = TicketType;
                LabelTicketCategory.Text = TicketCategory;
                LabelTicketStatus.Text = TicketStatus;
                LabelPrice.Text = Price.ToString() + " SEK";
                LabelDate.Text = Date;
            }
        }

        public PassCellTemplate()
        {
            InitializeComponent();

            LabelTicketType.TextColor = Color.FromHex("#404040");
            LabelTicketCategory.TextColor = Color.FromHex("#404040");
            LabelTicketStatus.TextColor = Color.FromHex("#404040");
            LabelPrice.TextColor = Color.FromHex("#404040");
            LabelDate.TextColor = Color.FromHex("#404040");

            LabelTicketType.FontSize = 18;
            LabelTicketCategory.FontSize = 18;
            LabelTicketStatus.FontSize = 18;
            LabelPrice.FontSize = 18;
            LabelDate.FontSize = 18;

            LabelTicketType.FontAttributes = FontAttributes.Bold;
            LabelTicketCategory.FontAttributes = FontAttributes.Bold;
            LabelTicketStatus.FontAttributes = FontAttributes.Bold;
            LabelPrice.FontAttributes = FontAttributes.Bold;
            LabelDate.FontAttributes = FontAttributes.Bold;

            LabelTypeTitle.TextColor = Color.FromHex("#404040");
            LabelCategoryTitle.TextColor = Color.FromHex("#404040");
            LabelStatusTitle.TextColor = Color.FromHex("#404040");

            LabelTypeTitle.FontSize = 18;
            LabelCategoryTitle.FontSize = 18;
            LabelStatusTitle.FontSize = 18;

            LabelTypeTitle.Text = "- Type";
            LabelCategoryTitle.Text = "- Category";
            LabelStatusTitle.Text = "- Status";
        }
    }
}
