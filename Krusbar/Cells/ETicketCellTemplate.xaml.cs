﻿using Xamarin.Forms;

namespace Krusbar.Cells
{
    public class ETicket
    {
        public string Group { get; set; }
        public double Price { get; set; }
        public double Duration { get; set; }
    }

    public partial class ETicketCellTemplate : ViewCell
    {
        public static readonly BindableProperty GroupProperty = BindableProperty.Create("Group", typeof(string), typeof(ETicketCellTemplate), "Group");
        public static readonly BindableProperty PriceProperty = BindableProperty.Create("Price", typeof(double), typeof(ETicketCellTemplate), 0.0);
        public static readonly BindableProperty DurationProperty = BindableProperty.Create("Duration", typeof(double), typeof(ETicketCellTemplate), 0.0);

        public string Group
        {
            get { return (string)GetValue(GroupProperty); }
            set { SetValue(GroupProperty, value); }
        }

        public double Price
        {
            get { return (double)GetValue(PriceProperty); }
            set { SetValue(PriceProperty, value); }
        }

        public double Duration
        {
            get { return (double)GetValue(DurationProperty); }
            set { SetValue(DurationProperty, value); }
        }
        
        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext != null)
            {
                LabelGroup.Text = Group;
                LabelPrice.Text = Price.ToString() + " SEK";
                LabelDuration.Text = Duration.ToString() + " min.";
            }
        }

        public ETicketCellTemplate()
        {
            InitializeComponent();

            LabelGroup.TextColor = Color.FromHex("#404040");
            LabelPrice.TextColor = Color.FromHex("#404040");
            LabelDuration.TextColor = Color.FromHex("#404040");

            LabelGroup.FontSize = 18;
            LabelPrice.FontSize = 18;
            LabelDuration.FontSize = 18;

            LabelGroup.FontAttributes = FontAttributes.Bold;
            LabelPrice.FontAttributes = FontAttributes.Bold;
            LabelDuration.FontAttributes = FontAttributes.Bold;
        }
    }
}
