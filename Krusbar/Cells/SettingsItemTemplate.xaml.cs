﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace Krusbar.Cells
{
    public class Settings
    {
        public string IconName { get; set; }
        public string LanguageName { get; set; }
        public bool IsSelected { get; set; }
    }

    public partial class SettingsItemTemplate : ContentView
    {
        string IconNameProperty;
        string LanguageNameProperty;
        bool IsSelectedProperty;

        public string IconName
        {
            get { return IconNameProperty; }
            set { IconNameProperty = value;
                updateContent();
            }
        }

        public string LanguageName
        {
            get { return LanguageNameProperty; }
            set { LanguageNameProperty = value;
                updateContent();
            }
        }

        public bool IsSelected
        {
            get { return IsSelectedProperty; }
            set { IsSelectedProperty = value;
                ImageIsSelected.Source = IsSelected ? "radio_on" : "radio_off";
                LabelLanguageName.FontAttributes = IsSelected ? FontAttributes.Bold : FontAttributes.None;
            }
        }

        public SettingsItemTemplate()
        {
            InitializeComponent();

            LabelLanguageName.TextColor = Color.FromHex("#3d3d3d");

            LabelLanguageName.FontSize = 18;
        }

        void updateContent()
        {
            ImageIconName.Source = IconName;
            LabelLanguageName.Text = LanguageName;
            ImageIsSelected.Source = IsSelected ? "radio_on" : "radio_off";

            LabelLanguageName.FontAttributes = IsSelected ? FontAttributes.Bold : FontAttributes.None;
        }
    }
}
