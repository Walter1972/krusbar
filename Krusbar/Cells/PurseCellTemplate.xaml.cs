﻿using Xamarin.Forms;

namespace Krusbar.Cells
{
    public class Purse
    {
        public double Price { get; set; }
        public string Wallet { get; set; }
        public string Person { get; set; }
    }

    public partial class PurseCellTemplate : ViewCell
    {
        public static readonly BindableProperty PriceProperty = BindableProperty.Create("Price", typeof(double), typeof(PurseCellTemplate), 0.0);
        public static readonly BindableProperty WalletProperty = BindableProperty.Create("Wallet", typeof(string), typeof(PurseCellTemplate), "");
        public static readonly BindableProperty PersonProperty = BindableProperty.Create("Person", typeof(string), typeof(PurseCellTemplate), "");

        public double Price
        {
            get { return (double)GetValue(PriceProperty); }
            set { SetValue(PriceProperty, value); }
        }

        public string Wallet
        {
            get { return (string)GetValue(WalletProperty); }
            set { SetValue(WalletProperty, value); }
        }

        public string Person
        {
            get { return (string)GetValue(PersonProperty); }
            set { SetValue(PersonProperty, value); }
        }

        protected override void OnBindingContextChanged()
        {
            base.OnBindingContextChanged();

            if (BindingContext != null)
            {
                LabelPrice.Text = Price.ToString() + " SEK";
                LabelWallet.Text = Wallet;
                LabelPerson.Text = Person;
            }
        }

        public PurseCellTemplate()
        {
            InitializeComponent();

            LabelPrice.TextColor = Color.FromHex("#404040");
            LabelWallet.TextColor = Color.FromHex("#404040");
            LabelPerson.TextColor = Color.FromHex("#404040");

            LabelPrice.FontSize = 18;
            LabelWallet.FontSize = 18;
            LabelPerson.FontSize = 18;

            LabelPrice.FontAttributes = FontAttributes.Bold;
            LabelWallet.FontAttributes = FontAttributes.Bold;
            LabelPerson.FontAttributes = FontAttributes.Bold;
        }
    }

}
