﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Xamarin.Forms;

namespace Krusbar.MenuItems
{
    public class MasterPageItem : INotifyPropertyChanged
	{
        public event PropertyChangedEventHandler PropertyChanged;

		public string Title { get; set; }
        public string IconName { get; set; }
		public Type TargetType { get; set; }

        private bool selected;
        private Color _textColor;
        private string _iconSource;

        public string IconSource {
            get { return _iconSource; }
            set {
                _iconSource = value;

				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("IconSource"));
				}
            }
        }

		public Color TextColor
		{
			get { return _textColor; }
			set
			{
				_textColor = value;

				if (PropertyChanged != null)
				{
					PropertyChanged(this, new PropertyChangedEventArgs("TextColor"));
				}
			}
		}

		public void SetSelected(bool isSelected)
		{
            selected = isSelected;

			if (isSelected)
			{
				TextColor = Color.FromRgb(0.0, 86.0 / 255.0, 127.0 / 255.0);
                IconSource = IconName + "_active";
			}
			else
			{
				TextColor = Color.FromRgb(63.0 / 255.0, 63.0 / 255.0, 63.0 / 255.0);
                IconSource = IconName;
			}
		}
	}
}
